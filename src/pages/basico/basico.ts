import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ServiceBasico } from '../../servicios/service-basico';
import { Pregunta } from '../../models/pregunta';
import { NivelAlcanzado } from '../../componentes/nivel-alcanzado';

@IonicPage()
@Component({
  selector: 'page-basico',
  templateUrl: 'basico.html',
})
export class BasicoPage {
  pregunta: any = new Pregunta();
  cantidad: number = 0;
  alternativas = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public serviceBasico: ServiceBasico, public modalCtrl: ModalController) {
    this.getCantidadPreguntas();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BasicoPage');
  }

  select(item) {
    console.log("respuesta: ", item);
    if (item.estado === "correcto") {
      let newp;
      this.pregunta.estado = "true";
      this.serviceBasico.actualizarEstadoPregunta(this.pregunta);
      newp = Number(this.pregunta.codigo) + 1;
      this.getPregunta(newp);
    } else {
      console.log("err");
    }
  }

  getPregunta(codigo) {
    console.log("prep: ", this.cantidad);
    if (codigo <= this.cantidad) {
      try {
        this.serviceBasico.getPregunta(codigo).valueChanges().subscribe((r) => {
          let ins = this;
          ins.pregunta = r[0];
          console.log(ins.pregunta)
        let altTemp = ins.pregunta.alternativas;
          ins.alternativas = ins.serviceBasico.getArrayRandom(altTemp);
          let count = 0;
        /*  var intervalId = setInterval(function () {
            if (count < 4) {
              ins.alternativas = ins.serviceBasico.getArrayRandom(altTemp);
              count++;
              console.log("...................",count);
            } else {
              clearInterval(intervalId);
            }
          }, 10000);*/
        });
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log("FELICITACIONES !!");
      this.felicitaciones();
    }
  }

  getCantidadPreguntas() {
    this.serviceBasico.getAllPreguntas().valueChanges().subscribe((res) => {
      let listaPreguntas: Array<any> = res;
      this.cantidad = listaPreguntas.length;
      console.log("cantidad: ", this.cantidad);
      let codigo = this.getSiguientePregunta(listaPreguntas);
      this.getPregunta(codigo); // podemos recuperar el estado de la ultima pregnta contestada
    });
  }

  felicitaciones() {
    let modal = this.modalCtrl.create(NivelAlcanzado);
    modal.present();
  }

  getSiguientePregunta(array) {
    let index;
    for (let item of array) {
      console.log(item);
      index = array.find(x => x.estado === "false");
    }
    return index.codigo;
  }

}
