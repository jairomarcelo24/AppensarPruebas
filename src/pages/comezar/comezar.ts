import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasicoPage } from '../basico/basico';

@IonicPage()
@Component({
  selector: 'page-comezar',
  templateUrl: 'comezar.html',
})
export class ComezarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  basico(){
    this.navCtrl.push("BasicoPage");
  }


}
