import { Component } from '@angular/core';
import { ViewController, NavController, AlertController } from 'ionic-angular';
import { User } from '../models/user';

@Component({
    selector: 'nivel-alcanzado',
    templateUrl: 'nivel-alcanzado.html'
})

export class NivelAlcanzado {
    constructor(public navCtrl: NavController, public viewCtrl: ViewController,
        public alertCtrl: AlertController) { }

    closeModal() {
        this.viewCtrl.dismiss();
    }

}