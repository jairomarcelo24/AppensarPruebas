import { Component } from '@angular/core';
import { NavController , NavParams} from 'ionic-angular';
import { RegistroPage } from '../registro/registro';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule , AngularFireDatabase } from 'angularfire2/database';

import { AngularFireAuth } from 'angularfire2/auth';
import { IniciarsesionPage } from '../iniciarsesion/iniciarsesion';
import { MyApp } from '../../app/app.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  slides = [
    {
      title: "Te damos la bienvenida",
      description: "Registrate para poder descubrir que tanto conoces sobre cultura general",
    },
    {
      title: "Aprende con tus amigos",
      description: "Arma grupos con tus amigos y compitan por quien sabe màs!",
    },
    {
      title: "Diviertete aprendiendo",
      description: "Diviértete y enriquece tu cerebro respondiendo preguntas de cultura general",
    }
  ];
  constructor(public navCtrl: NavController, public navParams:NavParams) {

  }
  public iniciarSesion() {
    this.navCtrl.push(IniciarsesionPage);
  }
  public cuenta() {
    this.navCtrl.push(RegistroPage);
  }



}
