export class Pregunta{
    codigo:number
    titulo:string
    imagen:string
    estado:boolean
    alternativas:Array<Alternativa>
}

export class Alternativa{
    estado:string
    nombre:string
}