import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import { User } from '../../models/user';
import { HomePage } from '../home/home';
import { Persona } from '../../models/persona';

@IonicPage()
@Component({
  selector: 'page-iniciarsesion',
  templateUrl: 'iniciarsesion.html',
})
export class IniciarsesionPage {

  popupSesion = false;
  informacion: Persona = {
    nombre: "",
    photo: ""
  };
  user = {} as User;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fireBaseAuth: AngularFireAuth) {
  }
  public iniciarFacebook() {

    this.fireBaseAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(
      res => {
        this.popupSesion = true;
        this.informacion.nombre = res.user.displayName;
        this.informacion.photo = res.user.photoURL;
        this.navCtrl.setRoot("ComezarPage", {
          informacion: this.informacion
        });
        console.log(res);
      }
    )

  }
  async ingresarMenu(user: User) {
    try {
      const result = this.fireBaseAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if (result) {
        this.navCtrl.push("ComezarPage");
      }

    } catch (e) {
      console.error(e);
    }


  }

}
