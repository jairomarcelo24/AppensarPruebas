import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireAuthModule } from 'angularfire2/auth';
import { RegistroPage } from '../pages/registro/registro';
import { IniciarsesionPage } from '../pages/iniciarsesion/iniciarsesion';
import { ServiceBasico } from '../servicios/service-basico';
import {NivelAlcanzado } from '../componentes/nivel-alcanzado'; 

export const firebaseConfig = {
  apiKey: "AIzaSyCa42ewCZtqCCTU6_ab1wRNVGQ4M77s52Y",
  authDomain: "appalcolev1.firebaseapp.com",
  databaseURL: "https://appalcolev1.firebaseio.com",
  projectId: "appalcolev1",
  storageBucket: "appalcolev1.appspot.com",
  messagingSenderId: "756924361361"
};
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegistroPage,
    IniciarsesionPage,
    NivelAlcanzado
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegistroPage,
    IniciarsesionPage,
    NivelAlcanzado
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler,  }, ServiceBasico,

  ]
})
export class AppModule { }
