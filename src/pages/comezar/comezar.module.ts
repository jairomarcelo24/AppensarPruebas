import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComezarPage } from './comezar';

@NgModule({
  declarations: [
    ComezarPage,
  ],
  imports: [
    IonicPageModule.forChild(ComezarPage),
  ],
})
export class ComezarPageModule {}
