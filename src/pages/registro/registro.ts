import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule , AngularFireDatabase, } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user';
import { Profile } from '../../models/profile';
import { HomePage } from '../home/home';
import firebase from 'firebase';
import { Persona } from '../../models/persona';


@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  user = {} as User;
  profile = {} as Profile;

  popupSesion = false;
  informacion: Persona = {
    nombre: "",
    photo: ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,private Aauth:AngularFireAuth, public afDatabase:AngularFireDatabase) {
  }
  public registratefacebook(){
    
        this.Aauth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(
          res =>{
            this.popupSesion = true;
            this.informacion.nombre = res.user.displayName;
            this.informacion.photo = res.user.photoURL;
            this.navCtrl.push("ComenzarPage",{
              informacion: this.informacion
            });
            console.log(res);
          }
        )
    
      }

  async crearCuenta(user:User){
    try{
    
      this.Aauth.authState.take(1).subscribe(auth =>{
        this.afDatabase.object((`profile/${auth.uid}`)).set(
          this.profile).then(() => this.navCtrl.setRoot("ComenzarPage"))
        ;
      })
      const result = this.Aauth.auth.createUserWithEmailAndPassword(user.email,user.password);
      
    }catch(e){
      console.error(e);
    }

  }
  
}
