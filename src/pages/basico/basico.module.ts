import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BasicoPage } from './basico';

@NgModule({
  declarations: [
    BasicoPage,
  ],
  imports: [
    IonicPageModule.forChild(BasicoPage),
  ],
})
export class BasicoPageModule {}
