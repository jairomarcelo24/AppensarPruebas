import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Pregunta } from '../models/pregunta';

@Injectable()
export class ServiceBasico {
    constructor(private fireService: AngularFireDatabase) { }

    getListaPreguntas() {
        return this.fireService.list('preguntas/basico/', resp => resp.orderByChild('estado').equalTo('false'))
    }

    getPregunta(codigo: number) {
        console.log(codigo);
        //return this.fireService.object('preguntas/basico/' + codigo);
        return this.fireService.list('preguntas/basico/', resp => resp.orderByChild('codigo').equalTo(codigo))
    }

    getAllPreguntas() {
        return this.fireService.list('preguntas/basico/');
    }

    actualizarEstadoPregunta(pregunta: Pregunta) {
        console.log("service: ", pregunta);
        this.fireService.object('preguntas/basico/' + String(pregunta.codigo)).update({
            estado: pregunta.estado
        }).then((success) => {
            console.log("success");
        }).catch((error) => {
            console.log("error");
        });
    }

    validarRespuesta(pregunta: Pregunta, respuesta: any): boolean {
        return true;
    }

    getArrayRandom(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

}